﻿Public Class Form1
    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles hora.ValueChanged
        FechaPersonalizada()
    End Sub

    Public Sub FechaPersonalizada()
        ' Fecha personalizada
        hora.Format = DateTimePickerFormat.Custom
        hora.Value = Now
        hora.CustomFormat = "HH:mm:ss"
    End Sub 'FechaPersonalizada

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles limpiar.Click
        equipoLocal.SelectedIndex = -1
        equipoVisitante.SelectedIndex = -1
        fecha.Text = ""
        puntosLocal.Text = ""
        puntosVisitante.Text = ""
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles salir.Click
        Close()
    End Sub

    Private Sub selectFecha_DateChanged(sender As Object, e As DateRangeEventArgs) Handles selectFecha.DateChanged
        fecha.Text = selectFecha.SelectionStart
    End Sub


    Private Sub fecha_MouseClick(sender As Object, e As MouseEventArgs) Handles fecha.MouseClick
        selectFecha.Visible = True
    End Sub


    Private Sub fecha_LostFocus(sender As Object, e As EventArgs) Handles fecha.LostFocus
        selectFecha.Visible = False
    End Sub

    Private Sub siguiente_Click(sender As Object, e As EventArgs) Handles siguiente.Click
        MsgBox("Partido: " + equipoLocal.Text + " " + puntosLocal.Value.ToString + " - " + equipoVisitante.Text + " " + puntosVisitante.Value.ToString + vbLf + "Jornada: " + fecha.Text + " " + hora.Value)
    End Sub

End Class
