﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.equipoLocal = New System.Windows.Forms.ComboBox()
        Me.equipoVisitante = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.selectFecha = New System.Windows.Forms.MonthCalendar()
        Me.fecha = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.hora = New System.Windows.Forms.DateTimePicker()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.puntosLocal = New System.Windows.Forms.NumericUpDown()
        Me.puntosVisitante = New System.Windows.Forms.NumericUpDown()
        Me.limpiar = New System.Windows.Forms.Button()
        Me.siguiente = New System.Windows.Forms.Button()
        Me.salir = New System.Windows.Forms.Button()
        CType(Me.puntosLocal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.puntosVisitante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(30, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Equipo local:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 85)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Equipo visitante:"
        '
        'equipoLocal
        '
        Me.equipoLocal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.equipoLocal.FormattingEnabled = True
        Me.equipoLocal.Items.AddRange(New Object() {"Valencia Basket", "Baskonia", "Gipuzkoa", "Joventut", "Gran Canaria", "Fuenlabrada", "Real Betis", "Tecnyconta Zaragoza", "MoraBanc Andorra", "Unicaja", "Barcelona", "Real Madrid", "UCAM Murcia", "San Pablo Burgos", "Bilbao Basket", "Iberostar Tenerife", "Obradoiro", "Real Betis"})
        Me.equipoLocal.Location = New System.Drawing.Point(122, 34)
        Me.equipoLocal.Name = "equipoLocal"
        Me.equipoLocal.Size = New System.Drawing.Size(224, 21)
        Me.equipoLocal.TabIndex = 2
        '
        'equipoVisitante
        '
        Me.equipoVisitante.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.equipoVisitante.FormattingEnabled = True
        Me.equipoVisitante.Items.AddRange(New Object() {"Valencia Basket", "Baskonia", "Gipuzkoa", "Joventut", "Gran Canaria", "Fuenlabrada", "Real Betis", "Tecnyconta Zaragoza", "MoraBanc Andorra", "Unicaja", "Barcelona", "Real Madrid", "UCAM Murcia", "San Pablo Burgos", "Bilbao Basket", "Iberostar Tenerife", "Obradoiro", "Real Betis"})
        Me.equipoVisitante.Location = New System.Drawing.Point(122, 82)
        Me.equipoVisitante.Name = "equipoVisitante"
        Me.equipoVisitante.Size = New System.Drawing.Size(224, 21)
        Me.equipoVisitante.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(33, 124)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(40, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Fecha:"
        '
        'selectFecha
        '
        Me.selectFecha.Location = New System.Drawing.Point(257, 121)
        Me.selectFecha.Name = "selectFecha"
        Me.selectFecha.TabIndex = 5
        Me.selectFecha.Visible = False
        '
        'fecha
        '
        Me.fecha.Location = New System.Drawing.Point(122, 121)
        Me.fecha.Name = "fecha"
        Me.fecha.Size = New System.Drawing.Size(123, 20)
        Me.fecha.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(33, 163)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(33, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Hora:"
        '
        'hora
        '
        Me.hora.CustomFormat = "HH:mm:ss"
        Me.hora.Location = New System.Drawing.Point(122, 163)
        Me.hora.Name = "hora"
        Me.hora.Size = New System.Drawing.Size(123, 20)
        Me.hora.TabIndex = 8
        Me.hora.Value = New Date(2018, 1, 23, 20, 17, 16, 0)
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(33, 200)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Puntos local"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(34, 238)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 13)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Puntos visitante"
        '
        'puntosLocal
        '
        Me.puntosLocal.Location = New System.Drawing.Point(122, 200)
        Me.puntosLocal.Maximum = New Decimal(New Integer() {300, 0, 0, 0})
        Me.puntosLocal.Name = "puntosLocal"
        Me.puntosLocal.Size = New System.Drawing.Size(45, 20)
        Me.puntosLocal.TabIndex = 11
        '
        'puntosVisitante
        '
        Me.puntosVisitante.Location = New System.Drawing.Point(122, 236)
        Me.puntosVisitante.Maximum = New Decimal(New Integer() {300, 0, 0, 0})
        Me.puntosVisitante.Name = "puntosVisitante"
        Me.puntosVisitante.Size = New System.Drawing.Size(45, 20)
        Me.puntosVisitante.TabIndex = 12
        '
        'limpiar
        '
        Me.limpiar.Location = New System.Drawing.Point(55, 302)
        Me.limpiar.Name = "limpiar"
        Me.limpiar.Size = New System.Drawing.Size(96, 33)
        Me.limpiar.TabIndex = 13
        Me.limpiar.Text = "Limpiar"
        Me.limpiar.UseVisualStyleBackColor = True
        '
        'siguiente
        '
        Me.siguiente.Location = New System.Drawing.Point(187, 302)
        Me.siguiente.Name = "siguiente"
        Me.siguiente.Size = New System.Drawing.Size(93, 33)
        Me.siguiente.TabIndex = 14
        Me.siguiente.Text = "Siguiente"
        Me.siguiente.UseVisualStyleBackColor = True
        '
        'salir
        '
        Me.salir.Location = New System.Drawing.Point(311, 302)
        Me.salir.Name = "salir"
        Me.salir.Size = New System.Drawing.Size(96, 33)
        Me.salir.TabIndex = 15
        Me.salir.Text = "Salir"
        Me.salir.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(488, 350)
        Me.Controls.Add(Me.salir)
        Me.Controls.Add(Me.siguiente)
        Me.Controls.Add(Me.limpiar)
        Me.Controls.Add(Me.puntosVisitante)
        Me.Controls.Add(Me.puntosLocal)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.hora)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.fecha)
        Me.Controls.Add(Me.selectFecha)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.equipoVisitante)
        Me.Controls.Add(Me.equipoLocal)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.puntosLocal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.puntosVisitante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents equipoLocal As ComboBox
    Friend WithEvents equipoVisitante As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents selectFecha As MonthCalendar
    Friend WithEvents fecha As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents hora As DateTimePicker
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents puntosLocal As NumericUpDown
    Friend WithEvents puntosVisitante As NumericUpDown
    Friend WithEvents limpiar As Button
    Friend WithEvents siguiente As Button
    Friend WithEvents salir As Button
End Class
